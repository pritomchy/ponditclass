import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.teal,
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircleAvatar(
                radius: 50,
                backgroundImage: AssetImage('assets/images/developer.jpeg'),
              ),
              Text(
                'Ebrahim Joy',
                style: TextStyle(
                    fontFamily: "Pacifico", fontSize: 40, color: Colors.white),
              ),
              Text(
                'FLUTTER DEVELOPER',
                style: TextStyle(
                    fontSize: 20,
                    color: Colors.teal.shade100,
                    letterSpacing: 2.5),
              ),
              SizedBox(
                height: 20,
                width: 150,
                child: Divider(
                  color: Colors.grey.shade50,
                  thickness: 2,
                ),
              ),
              Card(
                margin: EdgeInsets.symmetric(vertical: 10,horizontal: 25),
                child: ListTile(
                  leading: Icon(Icons.phone,
                  color: Colors.teal,),
                  title: Text('+8801515214189',style: TextStyle(
                    fontSize: 20,
                    color: Colors.teal.shade900,
                  ),),
                ),
              ),
              Card(
                margin: EdgeInsets.symmetric(vertical: 10,horizontal: 25),
                child: ListTile(
                  leading: Icon(Icons.email,
                    color: Colors.teal,),
                  title: Text('ebrahimjoy@gmail.com',style: TextStyle(
                    fontSize: 20,
                    color: Colors.teal.shade900,
                  ),),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
